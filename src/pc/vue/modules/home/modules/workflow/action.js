export default {
  methods: {
    /**
         * 加星
         */
    doStar(item, index) {
      const me = this;
      JE.ajax({
        url: '/je/jbpm/taskInfo/updateTaskPriority',
        params: {
          taskId: item.taskId,
          isNandY: this.isNandY,
          priority: item.priority == '1' ? '0' : '1',
        },
        async: true,
        success() {
          me.refresh();
        },
      });
    },
    /**
         * 延迟
         */
    doDelay(item, index) {
      const me = this; const
        { delay } = item;
      JE.ajax({
        url: '/je/jbpm/taskInfo/updateTaskDelay',
        params: {
          taskId: item.taskId,
          delay: delay == '1' ? '0' : '1',
        },
        async: true,
        success() {
          me.refresh();
          me.$emit('refreshItem', item.delay == '1' ? 'PREAPPROV' : 'DELAY');
        },
      });
    },
    /**
         * 查询参数
         */
    getParams() {
      const { type } = this; const
        pageInfo = this.getList().getPageInfo();
      const params = {
        start: pageInfo.start,
        limit: pageInfo.limit,
        page: pageInfo.page,
        queryType: `PI_${type}`,
        searchName: this.keyword,
        startDate: this.startDate,
        endDate: this.endDate,
        batchType: 0, // 批量
        isNandY: this.isNandY, // 是否完结
        wfKeys: '', // 流程类型
        whereSql: '',
      };
      // 获得流程类型节点的数据
      const node = this.getSelectNode();
      if (node) {
        const wfKeys = [];
        const nodes = node.nodeInfo == 'PROCESS' ? [node] : node.children;
        Ext.each(nodes, (n) => {
          n.nodeInfo == 'PROCESS' && wfKeys.push(n.code);
        });
        params.wfKeys = wfKeys.join(',');
        if (wfKeys.length > 0) {
          params.whereSql = `AND EXECUTION_ID_ IN (SELECT ID_ FROM JBPM4_EXECUTION WHERE PROCDEFID_ in (SELECT PROCESSINFO_PROCESSDEFINIT_ID FROM JE_CORE_PROCESSINFO WHERE PROCESSINFO_LASTVERSION!='none' AND PROCESSINFO_DEPLOYSTATUS='1' AND PROCESSINFO_ENABLED='1' AND PROCESSINFO_PROCESSKEY IN ('${
            wfKeys.join("','")
          }')))`;
          const j_query = {
            custom: [
              {
                code: 'EXECUTION_ID_',
                type: 'inSelect',
                value: {
                  table: 'JBPM4_EXECUTION',
                  code: 'ID_',
                  conditions: [{
                    code: 'PROCDEFID_',
                    type: 'inSelect',
                    value: {
                      table: 'JE_CORE_PROCESSINFO',
                      code: 'PROCESSINFO_PROCESSDEFINIT_ID',
                      conditions: [
                        { code: 'PROCESSINFO_LASTVERSION', value: 'none', type: '!=' },
                        { code: 'PROCESSINFO_DEPLOYSTATUS', value: '1', type: '=' },
                        { code: 'PROCESSINFO_ENABLED', value: '1', type: '=' },
                        { code: 'PROCESSINFO_PROCESSKEY', value: wfKeys, type: 'in' },
                      ],
                    },
                  }],
                },
              },
            ],
          };
          params.j_query = Ext.encode(j_query);
        }
      }
      return params;
    },
    /**
         * 加载数据
         */
    buildData(node) {
      const me = this;
      me.loading = true;
      JE.ajax({
        url: '/je/jbpm/taskInfo/getCurrentTask',
        params: me.getParams(node),
        async: true,
        success(response) {
          const data = JE.getAjaxData(response);
          me.data = data.rows;
          me.totalCount = data.totalCount;
          me.loading = false;
          me.$emit('refreshBadge', data, me.type);
        },
      });
    },
    refresh() {
      this.getList().refresh();
    },
    /**
         * 查看表单
         */
    itemClick(item, index) {
      const me = this;
      item.priority = '0';
      me.$set(me.data, index, item);
      const obj = JE.ajax({
        url: JE.getUrlMaps('je.core.getInfoById'), // 获得数据的地址
        params: {
          tableCode: item.tableCode,
          pkValue: item.pkValue,
        },
      });
      if (obj.success != false) {
        JE.showFunc(
          item.funcCode,
          // 写死,
          {
            useChild: true,
            values: obj,
            height: JE.getBodyHeight() - 50,
            width: JE.getBodyWidth() - 50,
            readOnly: false,
            type: 'form',
            callback(panel) {
              const win = panel.up('window');
              win.on('close', (win) => {
                const SY_PREAPPROVUSERS = panel.form.findField(
                  'SY_PREAPPROVUSERS'
                );
                const { wfInfo } = panel;
                if (!SY_PREAPPROVUSERS) {
                  return;
                }
                const preapprovUser = SY_PREAPPROVUSERS.getValue();
                let flag = true;
                // 如果待审批人有多个   则也刷新表格   因为会签操作和传阅操作可能有多个  也执行了。
                if (
                  !Ext.isEmpty(preapprovUser)
                                    && preapprovUser.split(',').indexOf(JE.currentUser.userId) != -1
                ) {
                  flag = false;
                  if (wfInfo && wfInfo.btns.indexOf('wfSubmitBtn') == -1) {
                    flag = true;
                  }
                }

                // 如果是会签   则判断当前表单如果含有改签则代表已处理会签。 刷新表格
                if (wfInfo && wfInfo.btns.indexOf('wfChangePassBtn') != -1) {
                  flag = true;
                }
                if (flag) {
                  // 加表格刷新
                  me.refresh();
                }
              });
            },
          }
        );
      }
    },
  },
};
