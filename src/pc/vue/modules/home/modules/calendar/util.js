/*
 * @Descripttion:
 * @version: V1.0.0
 * @Author: Shuangshuang Song
 * @Date: 2020-06-17 11:20:00
 * @LastEditors: Shuangshuang Song
 * @LastEditTime: 2020-07-10 13:56:24
 */


// 日历配置信息
const config = {
  solarMonth: new Array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31),
  weakStr: new Array('日', '一', '二', '三', '四', '五', '六'),
};

export default {
  calendar(y, m) {
    const me = this; const calendarObj = {};
    let lY; let lM; let lD = 1; let lL;
    let cY; let cM; let cD; // 年柱,月柱,日柱

    const sDObj = new Date(y, m, 1, 0, 0, 0, 0); // 当月一日日期

    calendarObj.length = me.solarDays(y, m); // 公历当月天数
    calendarObj.firstWeek = sDObj.getDay(); // 公历当月1日星期几

    for (let i = 0; i < calendarObj.length; i++) {
      const week = (i + calendarObj.firstWeek) % 7;
      calendarObj[i] = me.calElement(y, m + 1, i + 1, week,
        lY, lM, lD++, lL,
        cY, cM, cD);
    }

    const Today = new Date();
    const tY = Today.getFullYear();
    const tM = Today.getMonth();
    const tD = Today.getDate();

    // 今日
    if (y == tY && m == tM) calendarObj[tD - 1].isToday = true;
    return calendarObj;
  },
  calElement(sYear, sMonth, sDay, week) {
    const obj = {};
    obj.isToday = false;
    // 阳历
    obj.sYear = sYear; // 公元年4位数字
    obj.sMonth = sMonth; // 公元月数字
    obj.sDay = sDay; // 公元日数字
    obj.week = config.weakStr[week]; // 星期, 1个中文
    obj.weekNum = week;
    obj.sDate = `${sYear}-${sMonth > 9 ? sMonth : `0${sMonth}`}-${sDay > 9 ? sDay : `0${sDay}`}`;
    return obj;
  },
  //= =============================返回公历 y年某m+1月的天数
  solarDays(y, m) {
    if (m == 1) return (((y % 4 == 0) && (y % 100 != 0) || (y % 400 == 0)) ? 29 : 28);
    return (config.solarMonth[m]);
  },
  calendarWeeks(year, month) {
    const me = this;
    const obj = me.calendar(year, month);
    const dayNum = obj.length;// 天数
    let firstDay = 0; let lastDay = dayNum - 1;
    // 补全之前的天数
    let firstWeek = obj['0'].weekNum;
    firstWeek = firstWeek == 0 ? 7 : firstWeek;

    if (firstWeek > 1) {
      const _year = month == 0 ? year - 1 : year;
      const _month = month == 0 ? 11 : month - 1;
      const _obj = me.calendar(_year, _month);
      for (let i = 0; i < firstWeek - 1; i++) {
        firstDay = -i - 1;
        obj[firstDay] = _obj[_obj.length + firstDay];
        obj[firstDay].prevMonth = true;
      }
    }
    // 补全之后的天数
    let lastWeek = obj[dayNum - 1].weekNum;
    lastWeek = lastWeek == 0 ? 7 : lastWeek;
    if (lastWeek < 7) {
      const _year = month == 11 ? year + 1 : year;
      const _month = month == 11 ? 0 : month + 1;
      const _obj = me.calendar(_year, _month);
      for (let i = 0; i < 7 - lastWeek; i++) {
        lastDay = dayNum + i;
        obj[lastDay] = _obj[i];
        obj[lastDay].nextMonth = true;
      }
    }
    const weekNum = (lastDay + 1) / 7; const weeks = [];
    for (let i = 0; i < weekNum; i++) {
      const week = [];
      for (let j = 0; j < 7; j++) {
        const day = obj[i * 7 + j + firstDay];
        if (day.isToday && JE.isEmpty(me.activeDay)) {
          me.activeDay = day.sDay;
        }
        week.push(day);
      }
      weeks.push(week);
    }
    return weeks;
  },
};
