const m = {
  bubbleup: 'Please Post', // 冒个泡吧亲
  sayholder: 'What you want to say...', // 说你想说的(60字以内)...
  sayholder1: 'Say what you want to say (within 600 words)...', // 说你想说的(600字以内)...
  maopao: 'Post', // 冒泡
  maopao1: 'Post', // 冒 泡
  maopao2: ' Go ', // 冒泡
  zuixinmaopao: 'Lastest', // 最新冒泡
  zuiremaopao: 'Hottest', // 最热冒泡
  maopaoxiangqing: 'Bubble details', // 冒泡详情
  comment: 'Comment', // 评论
  somethingsay: 'I have something to say (less than 500 words)...', // 我有话要说(500字以内)...
  xuanzebiaoqing: 'Selective expression', // 选择表情
  xuanzetupian: 'Select pictures', // 选择图片
  remenmaopao: 'Popular bubble', // 热门冒泡
  floor: 'Floor', // 楼
  likes: 'Likes', // 点赞
  delete: 'Delete', // 删除
  reply: 'Reply', // 回复
};
export default m;
