/*
 * @Descripttion:
 * @Author: qinyonglian
 * @Date: 2019-11-22 13:40:48
 * @LastEditors: Shuangshuang Song
 * @LastEditTime: 2020-08-05 15:15:40
 */
export const m = {
  AccountLogin: 'Account login', // 账号登录
  QuickLogin: 'Quick login', // 快捷登录
  signIn: 'Go', // 登录Sign In
  RememberState: 'Remember', // 记住状态
  ForgetState: 'Forget', // 忘记密码
  DynamicPwd: 'Dynamic password', // 动态密码登录
  PersonalPwd: 'Personal password', // 个人密码登录
  tips: 'To use fast login, first go to personal settings and bind third party accounts', // 使用快捷登录，请先到个人设置中绑定第三方账号
  dingding: 'DingTalk', // 钉钉
  WeChat: 'WeChat', // 微信
  phoneholder: 'Mobile Phone Number/Account', // 手机号/账号
  msgholder: 'Short Message Verification Code', // 短信验证码
  pwdholder: 'Password', // 密码
  RetrievePwd: 'Retrieve Password', // 找回密码
  gain: 'Gain', // 获取
  next: 'Next', // 下一步
  pre: 'Previous step', // 上一步
  ReturnToLogin: 'Return to login', // 返回登录
  SelectEnterprise: 'Select enterprise', // 选择企业
  SelectEnterDept: 'Select enterdept', // 选择部门
  back: 'Back', // 返回
  backpre: 'Back', // 返回上一步
  RetrievePwd: 'Retrieve password', // 找回密码
  pwdalert: 'The password is a combination of letters and numbers of more than 6 digits', // 密码为6位以上字母+数字组合
  level1: 'Increasing the number of digits in a password can increase its strength', // 增加密码的位数，可以提升强度
  level2: 'The capitalization of letters enhances a level', // 字母的大小写，可加强一个等级
  level3: "It's already good. Please keep in mind", // 已经很不错了,请牢记
  newpwd: 'New password', // 新密码
  pwdconfirm: 'Confirm password', // 确认密码
  changeSuccess: 'Successful password modification', // 密码修改成功
  submit: 'Submit', // 提交
  RegisteredEnterpriseAccount: 'Registered Enterprise Account', // 注册企业账号
  phone: 'Cell-phone number', // 手机号
  shiyong: 'Log in using ', // 使用
  saomadenglu: ' Sweep Code', // 扫码登录
  tianxieshoujihao: 'Fill in the cell phone number', // 填写手机号
  tianxieqiyexinxi: 'Fill in enterprise information', // 填写企业信息
  zhucechenggong: 'Login was successful', // 注册成功
  yanzhengshoujihao: 'Verify the phone number (this phone number will be used as the personal account number at the time of login)', // 验证手机号（此手机号将作为登录时的个人帐号）
  yiyouzhanghaodenglu: 'Have an account to log in', // 已有账号去登录
  qudenglu: 'This mobile phone has been bound to the following enterprises. Click to log in', // 此手机已绑定以下企业，点击去登录
  creatnew: 'Create a new account', // 创建新账号
  qiyemingcheng: 'Enterprise name', // 企业名称
  realname: 'Real name', // 真实姓名
  email: 'Mailbox', // 邮箱
  industry: 'Industry', // 行业
  qiyezhanghao: 'Enterprise account number', // 企业账号
  name: 'Name', // 姓名
  gerenzhanghao: 'Personal account number', // 个人账号
  jinruwodeqiye: 'Enter my business', // 进入我的企业

};
